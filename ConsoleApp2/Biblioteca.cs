﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Biblioteca
    {
        List<LibroFiccion> librosFiccion;
        List<LibroInfantil> librosInfantiles;
        public Biblioteca(List<LibroFiccion> ficcion, List<LibroInfantil> infantil)
        {
            librosFiccion = ficcion;
            librosInfantiles = infantil;
        }

        public void addLibroInfantil(LibroInfantil infantil)
        {
            librosInfantiles.Add(infantil);
        }
        public void addLibroFiccion(LibroFiccion ficcion)
        {
            librosFiccion.Add(ficcion);
        }
        public void prestarLibroInfantil(LibroInfantil libro)
        {
            librosInfantiles.Remove(libro);
        }

        public void prestarLibroFiccion(LibroFiccion libro)
        {
            librosFiccion.Remove(libro);
        }
    }

    public class LibroFiccion
    {
        private string nombre;
        private string autor;
        private int precio;
        public LibroFiccion(string nombre, string autor, int precio)
        {
            this.nombre = nombre;
            this.autor = autor;
            this.precio = precio;
        }

        public string GetNombre()
        {
            return nombre;
        }

        public string GetAutor()
        {
            return autor;
        }

        public int GetPrecio()
        {
            return precio;
        }
    }

    public class LibroInfantil
    {
        private string nombre;
        private string autor;
        private int precio;

        public LibroInfantil(string nombre, string autor, int precio)
        {
            this.nombre = nombre;
            this.autor = autor;
            this.precio = precio;
        }

        public string GetNombre()
        {
            return nombre;
        }

        public string GetAutor()
        {
            return autor;
        }

        public int GetPrecio()
        {
            return precio;
        }
    }

    public class Persona
    {
        private string nombre;
        private int numero;
        private int ci;
        private List<LibroFiccion> librosFiccionP;
        private List<LibroInfantil> librosInfantilP;

        public Persona(string nombre, int numero, int ci)
        {
            this.nombre = nombre;
            this.numero = numero;
            this.ci = ci;
        }

        public string GetNombre()
        {
            return nombre;
        }
        public int GetNumero()
        {
            return numero;
        }
        public int GetCI()
        {
            return ci;
        }
        public void PrestarLibroFiccion(Biblioteca biblioteca, LibroFiccion ficcion)
        {
            biblioteca.prestarLibroFiccion(ficcion);
            if (librosFiccionP == null)
            {
                librosFiccionP = new List<LibroFiccion>();
            }
            librosFiccionP.Add(ficcion);
        }
        public void PrestarLibroInfantil(Biblioteca biblioteca, LibroInfantil infantil)
        {
            biblioteca.prestarLibroInfantil(infantil);
            if (librosInfantilP == null)
            {
                librosInfantilP = new List<LibroInfantil>();
            }
            librosInfantilP.Add(infantil);
        }
        
        public void DevolverLibroFiccion(Biblioteca biblioteca, LibroFiccion ficcion)
        {
            if (librosFiccionP.Remove(ficcion))
            {
                biblioteca.addLibroFiccion(ficcion);
            }
        }
        public void DevolverLibroInfantil(Biblioteca biblioteca, LibroInfantil infantil)
        {
            if (librosInfantilP.Remove(infantil))
            {
                biblioteca.addLibroInfantil(infantil);
            }
        }
    }
}
