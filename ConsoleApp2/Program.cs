﻿using System;
using System.Collections.Generic;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Biblioteca");

            LibroFiccion fic1 = new LibroFiccion("libro1", "author1", 100);
            LibroFiccion fic2 = new LibroFiccion("libro2", "author2", 200);
            LibroFiccion fic3 = new LibroFiccion("libro3", "author3", 300);
            LibroFiccion fic4 = new LibroFiccion("libro4", "author4", 400);

            List<LibroFiccion> librosFiccion = new List<LibroFiccion>();
            librosFiccion.Add(fic1);
            librosFiccion.Add(fic2);
            librosFiccion.Add(fic3);
            librosFiccion.Add(fic4);

            LibroInfantil inf1 = new LibroInfantil("libro10", "author5", 110);
            LibroInfantil inf2 = new LibroInfantil("libro11", "author6", 210);
            LibroInfantil inf3 = new LibroInfantil("libro12", "author7", 310);
            LibroInfantil inf4 = new LibroInfantil("libro13", "author8", 410);

            List<LibroInfantil> librosInfantiles = new List<LibroInfantil>();
            librosInfantiles.Add(inf1);
            librosInfantiles.Add(inf2);
            librosInfantiles.Add(inf3);
            librosInfantiles.Add(inf4);

            Biblioteca biblio = new Biblioteca(librosFiccion, librosInfantiles);

            Console.WriteLine("Lista de Libros");
            foreach (LibroFiccion libro in librosFiccion)
            {
                Console.WriteLine("Libro: {0} - Autor: {1} - Precio: {2}",libro.GetNombre(), libro.GetAutor(), libro.GetPrecio());
            }
            foreach (LibroInfantil libro in librosInfantiles)
            {
                Console.WriteLine("Libro: {0} - Autor: {1} - Precio: {2}", libro.GetNombre(), libro.GetAutor(), libro.GetPrecio());
            }

            Persona p1 = new Persona("Carlos", 77667766, 6677855);
            p1.PrestarLibroFiccion(biblio, fic1);
            p1.PrestarLibroFiccion(biblio, fic3);
            p1.PrestarLibroInfantil(biblio, inf2);
            p1.PrestarLibroInfantil(biblio, inf4);
            Console.WriteLine("\nLista de Libros actualizada");
            foreach (LibroFiccion libro in librosFiccion)
            {
                Console.WriteLine("Libro: {0} - Autor: {1} - Precio: {2}", libro.GetNombre(), libro.GetAutor(), libro.GetPrecio());
            }
            foreach (LibroInfantil libro in librosInfantiles)
            {
                Console.WriteLine("Libro: {0} - Autor: {1} - Precio: {2}", libro.GetNombre(), libro.GetAutor(), libro.GetPrecio());
            }
            Console.ReadKey();
        }
    }
}
